const gulp = require('gulp');
const { src, dest, watch, parallel } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const bs = require('browser-sync');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const concat = require('gulp-concat');
const pug = require('gulp-pug');
const htmlhint = require("gulp-htmlhint");
const changed = require("gulp-changed");
const imagemin = require("gulp-imagemin");
const recompress = require("imagemin-jpeg-recompress");
const pngquant = require("imagemin-pngquant");
const webpConv = require('gulp-webp');
const svgmin = require('gulp-svgmin');
const svgSprite = require('gulp-svg-sprite');
const ttf2woff2 = require('gulp-ttftowoff2');
const ttf2woff = require('gulp-ttf2woff');
// const spritesmith  = import('gulp.spritesmith');

gulp.task('ttf', function (done) {
  src('./dev/fonts/**/*.ttf')
    .pipe(changed('./assets/fonts', {
      extension: '.woff2',
      hasChanged: changed.compareLastModifiedTime
    }))
    .pipe(ttf2woff2())
    .pipe(dest('./assets/fonts'))

  src('./dev/fonts/**/*.ttf')
    .pipe(changed('./assets/fonts', {
      extension: 'woff',
      hasChanged: changed.compareLastModifiedTime
    }))
    .pipe(ttf2woff())
    .pipe(dest('./assets/fonts'))
  done();
  return;
});

gulp.task('htmlhint', function () {
  return gulp.src("*.html")
    .pipe(htmlhint())
    .pipe(htmlhint.reporter())
});

gulp.task('views', function () {
  return gulp.src('./dev/views/*.pug')
    .pipe(pug({pretty: true}))
    .pipe(gulp.dest('./'))
    .pipe(bs.stream())
});

gulp.task('svg_sprite', function () {
  return src('./dev/img/svg_sprite/**/*.svg')
    .pipe(svgmin({
      plugins: [{},
        'cleanupListOfValues'
      ]
    }))
    .pipe(svgSprite({
      mode: {
        stack: {
          sprite: 'sprite.svg'
        }
      }
    }))
    .pipe(dest('./assets/img'))
});

gulp.task('rastr', function () {
  return gulp.src('./dev/img/*.+(png|jpg|jpeg|gif|svg|ico)')
    .pipe(changed('./assets/img'))
      .pipe(imagemin({
          interlaced: true,
          progressive: true,
          optimizationLevel: 5,
        },
        [
          recompress({
            loops: 6,
            min: 50,
            max: 90,
            quality: 'high',
            use: [pngquant({
              quality: [0.8, 1],
              strip: true,
              speed: 1
            })],
          }),
          imagemin.optipng(),
          imagemin.svgo()
        ], ), )
      .pipe(dest('./assets/img'))
      .pipe(bs.stream())
});

gulp.task('webp', function(){
  return src('./assets/img/**/*.+(png|jpg|jpeg)')
    // .pipe(plumber())
    .pipe(changed('./assets/img', {
      extension: '.webp'
    }))
    .pipe(webpConv())
    .pipe(dest('./assets/img'))
});

// // gulp.task('sprite', function () {
// //   var spriteData = gulp.src('img/icons4sprite/*.*').pipe(spritesmith({
// //     imgName: 'sprite.png',
// //     cssName: '_sprite.scss',
// //     imgPath: './img/sprite.png',
// //     padding: 2,
// //   }));
// //   spriteData.img.pipe(gulp.dest('./img/'));
// //   spriteData.css.pipe(gulp.dest('./dev/sass/'));
// // });
 
gulp.task('styles', function () {
  return gulp.src('./dev/sass/*.scss')
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(rename({suffix: '.min', prefix : ''}))
    .pipe(autoprefixer({browsers: ['last 8 versions'], cascade: false}))
    .pipe(cleanCSS())
    .pipe(gulp.dest('./assets/css'))
    .pipe(bs.stream());
});

gulp.task('scripts', function() {
  return gulp.src([
    './dev/libs/jquery.min.js',
    './dev/libs/*.js',
    ])
    .pipe(concat('libs.js'))
    .pipe(gulp.dest('./assets/js/'));
});

gulp.task('browser-sync', function() {
    bs({server: {baseDir: "./"}});
});

gulp.task('watch', function () {
    gulp.watch('./dev/sass/**/*.scss', gulp.series('styles'));
    gulp.watch('./dev/libs/*.js', gulp.series('scripts'));
    gulp.watch('./js/*.js').on("change", bs.stream);
    gulp.watch("./dev/views/**/*.pug", gulp.series('views', 'htmlhint'));
    gulp.watch("./*.html").on('change', bs.stream);
    gulp.watch(["./dev/img/**/*.+(png|jpg|jpeg|gif|svg|ico)", "!!./dev/img/svg_sprite/*.*"], gulp.series('rastr', 'webp'));
    gulp.watch("./dev/img/svg_sprite/**/*.svg", gulp.series('svg_sprite'));
    gulp.watch("./dev/fonts/*.ttf", gulp.series('ttf'));
});

gulp.task('default', gulp.parallel('watch', 'rastr', 'styles', 'webp', 'svg_sprite', 'ttf', 'browser-sync'));
